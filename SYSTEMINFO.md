# Zscaler

Vendor: Zscaler
Homepage: https://www.zscaler.com/

Product: Internet Access (ZIA)
Product Page: https://www.zscaler.com/products-and-solutions/zscaler-internet-access

## Introduction
We classify Zscaler into the Security domain as Zscaler provides access to cloud security services and functionalities. 

"Zscaler operates the world's largest security-as-a-service (SaaS) cloud platform to provide the industry's only 100% cloud-delivered web and mobile security solution."

## Why Integrate
The Zscaler adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Zscaler Internet Access (ZIA) to provides benefits and use cases related to enhancing network security, automation and orchestration capabilities.

With this adapter you have the ability to perform operations with Zscaler such as:

- Security Policies
- Manage user authentication and access controls

## Additional Product Documentation
The [API documents for Zscaler](https://help.zscaler.com/zia)