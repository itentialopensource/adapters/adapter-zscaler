## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Zscaler. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Zscaler.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Adapter for Zscaler. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getAuthenticatedSession(callback)</td>
    <td style="padding:15px">Checks if there is an authenticated session</td>
    <td style="padding:15px">{base_path}/{version}/authenticatedSession?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthenticatedSession(credentials, callback)</td>
    <td style="padding:15px">Creates an authenticated session</td>
    <td style="padding:15px">{base_path}/{version}/authenticatedSession?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthenticatedSession(callback)</td>
    <td style="padding:15px">Ends an authenticated session</td>
    <td style="padding:15px">{base_path}/{version}/authenticatedSession?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditReport(callback)</td>
    <td style="padding:15px">This endpoint is deprecated, use GET /auditlogEntryReport</td>
    <td style="padding:15px">{base_path}/{version}/auditReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuditReport(callback)</td>
    <td style="padding:15px">This endpoint is deprecated, use POST /auditlogEntryReport</td>
    <td style="padding:15px">{base_path}/{version}/auditReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuditReport(callback)</td>
    <td style="padding:15px">This endpoint is deprecated, use DELETE /auditlogEntryReport</td>
    <td style="padding:15px">{base_path}/{version}/auditReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditReportdownload(callback)</td>
    <td style="padding:15px">This endpoint is deprecated, use GET /auditlogEntryReport/download</td>
    <td style="padding:15px">{base_path}/{version}/auditReport/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditlogEntryReport(callback)</td>
    <td style="padding:15px">Gets the status of a request for an audit log report</td>
    <td style="padding:15px">{base_path}/{version}/auditlogEntryReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuditlogEntryReport(request, callback)</td>
    <td style="padding:15px">Creates an audit log report for the specified time period and saves it as a CSV file</td>
    <td style="padding:15px">{base_path}/{version}/auditlogEntryReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuditlogEntryReport(callback)</td>
    <td style="padding:15px">Cancels the request to create an audit log report</td>
    <td style="padding:15px">{base_path}/{version}/auditlogEntryReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuditlogEntryReportdownload(callback)</td>
    <td style="padding:15px">Downloads the most recently created audit log report</td>
    <td style="padding:15px">{base_path}/{version}/auditlogEntryReport/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDepartments(callback)</td>
    <td style="padding:15px">Gets a list of departments</td>
    <td style="padding:15px">{base_path}/{version}/departments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDepartmentsid(id, callback)</td>
    <td style="padding:15px">Gets the department for the specified ID</td>
    <td style="padding:15px">{base_path}/{version}/departments/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroups(search, page, pageSize, callback)</td>
    <td style="padding:15px">Gets a list of groups</td>
    <td style="padding:15px">{base_path}/{version}/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsgroupId(groupId, callback)</td>
    <td style="padding:15px">Gets the group for the specified ID</td>
    <td style="padding:15px">{base_path}/{version}/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(name, dept, group, page, pageSize, callback)</td>
    <td style="padding:15px">Gets a list of all users and allows user filtering by name, department, or group</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsers(user, callback)</td>
    <td style="padding:15px">Adds a new user</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsersbulkDelete(userIds, callback)</td>
    <td style="padding:15px">Bulk delete users up to a maximum of 500 users per request</td>
    <td style="padding:15px">{base_path}/{version}/users/bulkDelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersuserId(userId, callback)</td>
    <td style="padding:15px">Gets the user information for the specified ID</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUsersuserId(userId, user, callback)</td>
    <td style="padding:15px">Updates the user information for the specified ID</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersuserId(userId, callback)</td>
    <td style="padding:15px">Deletes the user for the specified ID</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurity(callback)</td>
    <td style="padding:15px">Gets a list of white-listed URLs</td>
    <td style="padding:15px">{base_path}/{version}/security?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSecurity(policy, callback)</td>
    <td style="padding:15px">Updates the list of white-listed URLs</td>
    <td style="padding:15px">{base_path}/{version}/security?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecurityadvanced(callback)</td>
    <td style="padding:15px">Gets a list of black-listed URLs</td>
    <td style="padding:15px">{base_path}/{version}/security/advanced?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putSecurityadvanced(advSettings, callback)</td>
    <td style="padding:15px">Updates the list of black-listed URLs</td>
    <td style="padding:15px">{base_path}/{version}/security/advanced?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecurityadvancedblacklistUrls(action, callback)</td>
    <td style="padding:15px">Adds a URL to or removes a URL from the black list</td>
    <td style="padding:15px">{base_path}/{version}/security/advanced/blacklistUrls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSslSettingscertchain(callback)</td>
    <td style="padding:15px">Deletes the intermediate certificate chain</td>
    <td style="padding:15px">{base_path}/{version}/sslSettings/certchain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSslSettingsdownloadcsr(callback)</td>
    <td style="padding:15px">Downloads a Certificate Signing Request (CSR)</td>
    <td style="padding:15px">{base_path}/{version}/sslSettings/downloadcsr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSslSettingsgeneratecsr(cert, callback)</td>
    <td style="padding:15px">Generates a Certificate Signing Request (CSR)</td>
    <td style="padding:15px">{base_path}/{version}/sslSettings/generatecsr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSslSettingsshowcert(callback)</td>
    <td style="padding:15px">Shows information about the signed intermediate root certificate</td>
    <td style="padding:15px">{base_path}/{version}/sslSettings/showcert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSslSettingsuploadcerttext(fileInputStream, callback)</td>
    <td style="padding:15px">Uploads a signed intermediate root certificate for clients that use iframe-based uploads whose content type is text/plain</td>
    <td style="padding:15px">{base_path}/{version}/sslSettings/uploadcert/text?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSslSettingsuploadcertchaintext(fileInputStream, callback)</td>
    <td style="padding:15px">Uploads the Intermediate Certificate Chain (PEM) for clients that use iframe-based uploads whose content type is text/plain</td>
    <td style="padding:15px">{base_path}/{version}/sslSettings/uploadcertchain/text?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUrlCategoriesWithOptions(queryData, callback)</td>
    <td style="padding:15px">Gets information about all or custom URL categories with query data</td>
    <td style="padding:15px">{base_path}/{version}/urlCategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUrlCategories(customCategory, callback)</td>
    <td style="padding:15px">Adds a new custom URL category</td>
    <td style="padding:15px">{base_path}/{version}/urlCategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUrlCategorieslite(callback)</td>
    <td style="padding:15px">Gets a lightweight key-value list of all or custom URL categories</td>
    <td style="padding:15px">{base_path}/{version}/urlCategories/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUrlCategoriesurlQuota(callback)</td>
    <td style="padding:15px">Gets the URL quota information for your organization</td>
    <td style="padding:15px">{base_path}/{version}/urlCategories/urlQuota?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUrlCategoriescategoryId(categoryId, callback)</td>
    <td style="padding:15px">Gets the URL category information for the specified ID</td>
    <td style="padding:15px">{base_path}/{version}/urlCategories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUrlCategoriescategoryId(categoryId, action, customCategory, callback)</td>
    <td style="padding:15px">Updates the URL category for the specified ID</td>
    <td style="padding:15px">{base_path}/{version}/urlCategories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUrlCategoriescategoryId(categoryId, callback)</td>
    <td style="padding:15px">Deletes the custom URL category for the specified ID</td>
    <td style="padding:15px">{base_path}/{version}/urlCategories/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUrlLookup(urls, callback)</td>
    <td style="padding:15px">Look up the categorization of the given set of URLs, e.g., ['abc.com', 'xyz.com']</td>
    <td style="padding:15px">{base_path}/{version}/urlLookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVpnCredentials(search, type, includeOnlyWithoutLocation, locationId, managedBy, page, pageSize, callback)</td>
    <td style="padding:15px">Gets VPN credentials that can be associated to locations</td>
    <td style="padding:15px">{base_path}/{version}/vpnCredentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVpnCredentials(vpnCred, callback)</td>
    <td style="padding:15px">Adds VPN credentials that can be associated to locations</td>
    <td style="padding:15px">{base_path}/{version}/vpnCredentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVpnCredentialsbulkDelete(ids, callback)</td>
    <td style="padding:15px">Bulk delete VPN credentials up to a maximum of 100 credentials per request</td>
    <td style="padding:15px">{base_path}/{version}/vpnCredentials/bulkDelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVpnCredentialsvpnId(vpnId, callback)</td>
    <td style="padding:15px">Gets the VPN credentials for the specified ID</td>
    <td style="padding:15px">{base_path}/{version}/vpnCredentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVpnCredentialsvpnId(vpnId, vpnCred, callback)</td>
    <td style="padding:15px">Updates the VPN credentials for the specified ID</td>
    <td style="padding:15px">{base_path}/{version}/vpnCredentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVpnCredentialsvpnId(vpnId, callback)</td>
    <td style="padding:15px">Deletes the VPN credentials for the specified ID</td>
    <td style="padding:15px">{base_path}/{version}/vpnCredentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocations(search, sslScanEnabled, xffEnabled, authRequired, bwEnforced, page, pageSize, callback)</td>
    <td style="padding:15px">Gets information on locations</td>
    <td style="padding:15px">{base_path}/{version}/locations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLocations(location, callback)</td>
    <td style="padding:15px">Adds a new location</td>
    <td style="padding:15px">{base_path}/{version}/locations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLocationsbulkDelete(LocationIds, callback)</td>
    <td style="padding:15px">Bulk delete locations up to a maximum of 100 locations per request</td>
    <td style="padding:15px">{base_path}/{version}/locations/bulkDelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocationslite(sslScanEnabled, search, page, pageSize, callback)</td>
    <td style="padding:15px">Gets a lightweight list of locations</td>
    <td style="padding:15px">{base_path}/{version}/locations/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocationslocationId(locationId, callback)</td>
    <td style="padding:15px">Gets the location information for the specified ID</td>
    <td style="padding:15px">{base_path}/{version}/locations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putLocationslocationId(locationId, location, callback)</td>
    <td style="padding:15px">Updates the location information for the specified ID</td>
    <td style="padding:15px">{base_path}/{version}/locations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLocationslocationId(locationId, callback)</td>
    <td style="padding:15px">Deletes the location for the specified ID</td>
    <td style="padding:15px">{base_path}/{version}/locations/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSandboxreportquota(callback)</td>
    <td style="padding:15px">Gets the Sandbox Report API quota information for your organization</td>
    <td style="padding:15px">{base_path}/{version}/sandbox/report/quota?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSandboxreportmd5Hash(md5Hash, details, callback)</td>
    <td style="padding:15px">Gets a full (i.e., complete) or summary detail report for an MD5 hash of a file that was analyzed by Sandbox</td>
    <td style="padding:15px">{base_path}/{version}/sandbox/report/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatus(callback)</td>
    <td style="padding:15px">Gets the activation status for a configuration change</td>
    <td style="padding:15px">{base_path}/{version}/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatusactivate(callback)</td>
    <td style="padding:15px">Activates configuration changes</td>
    <td style="padding:15px">{base_path}/{version}/status/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">submitFile(apiToken, force, callback)</td>
    <td style="padding:15px">submitFile</td>
    <td style="padding:15px">{base_path}/{version}/zscsb/submit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">submitFileForScan(apiToken, callback)</td>
    <td style="padding:15px">submitFileForScan</td>
    <td style="padding:15px">{base_path}/{version}/zscsb/discan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApp(appId, url, verbose, callback)</td>
    <td style="padding:15px">getApp</td>
    <td style="padding:15px">{base_path}/{version}/apps/app?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApp(body, callback)</td>
    <td style="padding:15px">postApp</td>
    <td style="padding:15px">{base_path}/{version}/apps/app?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appsSearchGET(appName, limit, page, callback)</td>
    <td style="padding:15px">AppsSearch_GET</td>
    <td style="padding:15px">{base_path}/{version}/apps/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getViews(callback)</td>
    <td style="padding:15px">getViews</td>
    <td style="padding:15px">{base_path}/{version}/app_views/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getViewApps(appViewId, callback)</td>
    <td style="padding:15px">getViewApps</td>
    <td style="padding:15px">{base_path}/{version}/app_views/{pathv1}/apps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoleSummary(includeAuditorRole, includePartnerRole, includeApiRole, callback)</td>
    <td style="padding:15px">getRoleSummary</td>
    <td style="padding:15px">{base_path}/{version}/adminRoles/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdminUsers(includeAuditorUsers, includeAdminUsers, search, page, pageSize, callback)</td>
    <td style="padding:15px">getAdminUsers</td>
    <td style="padding:15px">{base_path}/{version}/adminUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addAdminUser(associateWithExistingAdmin, body, callback)</td>
    <td style="padding:15px">addAdminUser</td>
    <td style="padding:15px">{base_path}/{version}/adminUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAdminUser(userId, callback)</td>
    <td style="padding:15px">updateAdminUser</td>
    <td style="padding:15px">{base_path}/{version}/adminUsers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAdminUser(userId, callback)</td>
    <td style="padding:15px">deleteAdminUser</td>
    <td style="padding:15px">{base_path}/{version}/adminUsers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">convertAdminToEndUser(userId, body, callback)</td>
    <td style="padding:15px">convertAdminToEndUser</td>
    <td style="padding:15px">{base_path}/{version}/adminUsers/{pathv1}/convertToUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudBrowserIsolationProfiles(callback)</td>
    <td style="padding:15px">getCloudBrowserIsolationProfiles</td>
    <td style="padding:15px">{base_path}/{version}/browserIsolation/profiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDlpDictionaries(search, callback)</td>
    <td style="padding:15px">getDlpDictionaries</td>
    <td style="padding:15px">{base_path}/{version}/dlpDictionaries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCustomDLPDictionary(body, callback)</td>
    <td style="padding:15px">addCustomDLPDictionary</td>
    <td style="padding:15px">{base_path}/{version}/dlpDictionaries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDLPDictionarySummary(callback)</td>
    <td style="padding:15px">getDLPDictionarySummary</td>
    <td style="padding:15px">{base_path}/{version}/dlpDictionaries/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDlpDictionaryById(dlpDictId, callback)</td>
    <td style="padding:15px">getDlpDictionaryById</td>
    <td style="padding:15px">{base_path}/{version}/dlpDictionaries/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDlpDictionary(dlpDictId, body, callback)</td>
    <td style="padding:15px">updateDlpDictionary</td>
    <td style="padding:15px">{base_path}/{version}/dlpDictionaries/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomDlpDictionary(dlpDictId, callback)</td>
    <td style="padding:15px">deleteCustomDlpDictionary</td>
    <td style="padding:15px">{base_path}/{version}/dlpDictionaries/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPredefinedHierarchicalIdentifiersByDictionaryId(dictId, callback)</td>
    <td style="padding:15px">getPredefinedHierarchicalIdentifiersByDictionaryId</td>
    <td style="padding:15px">{base_path}/{version}/dlpDictionaries/{pathv1}/predefinedIdentifiers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateDlpPattern(callback)</td>
    <td style="padding:15px">validateDlpPattern</td>
    <td style="padding:15px">{base_path}/{version}/dlpDictionaries/validateDlpPattern?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDLPEngines(callback)</td>
    <td style="padding:15px">getDLPEngines</td>
    <td style="padding:15px">{base_path}/{version}/dlpEngines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCustomDLPEngine(body, callback)</td>
    <td style="padding:15px">addCustomDLPEngine</td>
    <td style="padding:15px">{base_path}/{version}/dlpEngines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDLPEnginesSummary(callback)</td>
    <td style="padding:15px">getDLPEnginesSummary</td>
    <td style="padding:15px">{base_path}/{version}/dlpEngines/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDlpEnginesValidateDlpExpr(body, callback)</td>
    <td style="padding:15px">validateDlpPattern</td>
    <td style="padding:15px">{base_path}/{version}/dlpEngines/validateDlpExpr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDLPEngineById(dlpEngineId, callback)</td>
    <td style="padding:15px">getDLPEngineById</td>
    <td style="padding:15px">{base_path}/{version}/dlpEngines/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDlpEngine(dlpEngineId, body, callback)</td>
    <td style="padding:15px">updateDlpEngine</td>
    <td style="padding:15px">{base_path}/{version}/dlpEngines/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomDlpEngine(dlpEngineId, callback)</td>
    <td style="padding:15px">deleteCustomDlpEngine</td>
    <td style="padding:15px">{base_path}/{version}/dlpEngines/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchemas(callback)</td>
    <td style="padding:15px">getSchemas</td>
    <td style="padding:15px">{base_path}/{version}/dlpExactDataMatchSchemas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSchemaLite(schemaName, activeOnly, fetchTokens, callback)</td>
    <td style="padding:15px">getSchemaLite</td>
    <td style="padding:15px">{base_path}/{version}/dlpExactDataMatchSchemas/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gettemplates(callback)</td>
    <td style="padding:15px">gettemplates</td>
    <td style="padding:15px">{base_path}/{version}/dlpNotificationTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addtemplate(callback)</td>
    <td style="padding:15px">addtemplate</td>
    <td style="padding:15px">{base_path}/{version}/dlpNotificationTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateById(templateId, callback)</td>
    <td style="padding:15px">getTemplateById</td>
    <td style="padding:15px">{base_path}/{version}/dlpNotificationTemplates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updatetemplate(templateId, callback)</td>
    <td style="padding:15px">updatetemplate</td>
    <td style="padding:15px">{base_path}/{version}/dlpNotificationTemplates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletetemplate(templateId, callback)</td>
    <td style="padding:15px">deletetemplate</td>
    <td style="padding:15px">{base_path}/{version}/dlpNotificationTemplates/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getICAPServers(callback)</td>
    <td style="padding:15px">getICAPServers</td>
    <td style="padding:15px">{base_path}/{version}/icapServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getICAPServerSummary(callback)</td>
    <td style="padding:15px">getICAPServerSummary</td>
    <td style="padding:15px">{base_path}/{version}/icapServers/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getICAPServer(icapServerId, callback)</td>
    <td style="padding:15px">getICAPServer</td>
    <td style="padding:15px">{base_path}/{version}/icapServers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProfiles(callback)</td>
    <td style="padding:15px">getProfiles</td>
    <td style="padding:15px">{base_path}/{version}/idmprofile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSummary(activeOnly, callback)</td>
    <td style="padding:15px">getSummary</td>
    <td style="padding:15px">{base_path}/{version}/idmprofile/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProfileById(profileId, callback)</td>
    <td style="padding:15px">getProfileById</td>
    <td style="padding:15px">{base_path}/{version}/idmprofile/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidentReceiverServers(callback)</td>
    <td style="padding:15px">getIncidentReceiverServers</td>
    <td style="padding:15px">{base_path}/{version}/incidentReceiverServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidentReceiverServerSummary(callback)</td>
    <td style="padding:15px">getIncidentReceiverServerSummary</td>
    <td style="padding:15px">{base_path}/{version}/incidentReceiverServers/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIncidentReceiverServer(incidentReceiverServerId, callback)</td>
    <td style="padding:15px">getIncidentReceiverServer</td>
    <td style="padding:15px">{base_path}/{version}/incidentReceiverServers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRules(callback)</td>
    <td style="padding:15px">getRules</td>
    <td style="padding:15px">{base_path}/{version}/webDlpRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRule(body, callback)</td>
    <td style="padding:15px">addRule</td>
    <td style="padding:15px">{base_path}/{version}/webDlpRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebDlpRulesLite(callback)</td>
    <td style="padding:15px">getSummary</td>
    <td style="padding:15px">{base_path}/{version}/webDlpRules/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRuleById(ruleId, callback)</td>
    <td style="padding:15px">getRuleById</td>
    <td style="padding:15px">{base_path}/{version}/webDlpRules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRule(ruleId, body, callback)</td>
    <td style="padding:15px">updateRule</td>
    <td style="padding:15px">{base_path}/{version}/webDlpRules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRule(ruleId, callback)</td>
    <td style="padding:15px">deleteRule</td>
    <td style="padding:15px">{base_path}/{version}/webDlpRules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroups(includeDeviceInfo, includePseudoGroups, callback)</td>
    <td style="padding:15px">getDeviceGroups</td>
    <td style="padding:15px">{base_path}/{version}/deviceGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevices(name, model, owner, osType, osVersion, deviceGroupId, userIds, searchAll, includeAll, page, pageSize, callback)</td>
    <td style="padding:15px">getDevices</td>
    <td style="padding:15px">{base_path}/{version}/deviceGroups/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesLite(name, userIds, includeAll, page, pageSize, callback)</td>
    <td style="padding:15px">getDevicesLite</td>
    <td style="padding:15px">{base_path}/{version}/deviceGroups/devices/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventReportEntryCsvExportStatus(callback)</td>
    <td style="padding:15px">getEventReportEntryCsvExportStatus</td>
    <td style="padding:15px">{base_path}/{version}/eventlogEntryReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startEventReportEntryCsvExport(body, callback)</td>
    <td style="padding:15px">startEventReportEntryCsvExport</td>
    <td style="padding:15px">{base_path}/{version}/eventlogEntryReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancelEventReportEntryCsvExport(callback)</td>
    <td style="padding:15px">cancelEventReportEntryCsvExport</td>
    <td style="padding:15px">{base_path}/{version}/eventlogEntryReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventReportEntryCsvData(callback)</td>
    <td style="padding:15px">getEventReportEntryCsvData</td>
    <td style="padding:15px">{base_path}/{version}/eventlogEntryReport/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallFilteringRules(callback)</td>
    <td style="padding:15px">getFirewallFilteringRules</td>
    <td style="padding:15px">{base_path}/{version}/firewallFilteringRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createFirewallFilteringRule(body, callback)</td>
    <td style="padding:15px">createFirewallFilteringRule</td>
    <td style="padding:15px">{base_path}/{version}/firewallFilteringRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFirewallFilteringRule(ruleId, callback)</td>
    <td style="padding:15px">getFirewallFilteringRule</td>
    <td style="padding:15px">{base_path}/{version}/firewallFilteringRules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateFirewallFilteringRule(ruleId, body, callback)</td>
    <td style="padding:15px">updateFirewallFilteringRule</td>
    <td style="padding:15px">{base_path}/{version}/firewallFilteringRules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteFirewallFilteringRule(ruleId, callback)</td>
    <td style="padding:15px">deleteFirewallFilteringRule</td>
    <td style="padding:15px">{base_path}/{version}/firewallFilteringRules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDestinationIpGroups(excludeType, callback)</td>
    <td style="padding:15px">getDestinationIpGroups</td>
    <td style="padding:15px">{base_path}/{version}/ipDestinationGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDestinationIpGroup(callback)</td>
    <td style="padding:15px">addDestinationIpGroup</td>
    <td style="padding:15px">{base_path}/{version}/ipDestinationGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDestinationIpv6Groups(excludeType, callback)</td>
    <td style="padding:15px">getDestinationIpv6Groups</td>
    <td style="padding:15px">{base_path}/{version}/ipDestinationGroups/ipv6DestinationGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDestinationIpv6GroupsLite(type, excludeType, callback)</td>
    <td style="padding:15px">getDestinationIpv6GroupsLite</td>
    <td style="padding:15px">{base_path}/{version}/ipDestinationGroups/ipv6DestinationGroups/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDestinationIpGroupsLite(type, excludeType, callback)</td>
    <td style="padding:15px">getDestinationIpGroupsLite</td>
    <td style="padding:15px">{base_path}/{version}/ipDestinationGroups/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDestinationIpGroupById(ipGroupId, callback)</td>
    <td style="padding:15px">getDestinationIpGroupById</td>
    <td style="padding:15px">{base_path}/{version}/ipDestinationGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editDestinationIpGroup(ipGroupId, callback)</td>
    <td style="padding:15px">editDestinationIpGroup</td>
    <td style="padding:15px">{base_path}/{version}/ipDestinationGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDestinationIpGroup(ipGroupId, callback)</td>
    <td style="padding:15px">deleteDestinationIpGroup</td>
    <td style="padding:15px">{base_path}/{version}/ipDestinationGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSourceIpGroups(search, callback)</td>
    <td style="padding:15px">getSourceIpGroups</td>
    <td style="padding:15px">{base_path}/{version}/ipSourceGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addSourceIpGroup(callback)</td>
    <td style="padding:15px">addSourceIpGroup</td>
    <td style="padding:15px">{base_path}/{version}/ipSourceGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSourceIpv6Groups(callback)</td>
    <td style="padding:15px">getSourceIpv6Groups</td>
    <td style="padding:15px">{base_path}/{version}/ipSourceGroups/ipv6SourceGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSourceIpv6GroupsLite(callback)</td>
    <td style="padding:15px">getSourceIpv6GroupsLite</td>
    <td style="padding:15px">{base_path}/{version}/ipSourceGroups/ipv6SourceGroups/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSourceIpGroupsLite(callback)</td>
    <td style="padding:15px">getSourceIpGroupsLite</td>
    <td style="padding:15px">{base_path}/{version}/ipSourceGroups/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSourceIpGroupById(ipGroupId, callback)</td>
    <td style="padding:15px">getSourceIpGroupById</td>
    <td style="padding:15px">{base_path}/{version}/ipSourceGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editSourceIpGroup(ipGroupId, callback)</td>
    <td style="padding:15px">editSourceIpGroup</td>
    <td style="padding:15px">{base_path}/{version}/ipSourceGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSourceIpGroup(ipGroupId, callback)</td>
    <td style="padding:15px">deleteSourceIpGroup</td>
    <td style="padding:15px">{base_path}/{version}/ipSourceGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetowrkApplicationGroups(search, callback)</td>
    <td style="padding:15px">getNetowrkApplicationGroups</td>
    <td style="padding:15px">{base_path}/{version}/networkApplicationGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkApplicationGroup(callback)</td>
    <td style="padding:15px">createNetworkApplicationGroup</td>
    <td style="padding:15px">{base_path}/{version}/networkApplicationGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetowrkApplicationGroupsLite(callback)</td>
    <td style="padding:15px">getNetowrkApplicationGroupsLite</td>
    <td style="padding:15px">{base_path}/{version}/networkApplicationGroups/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkApplicationGroupById(groupId, callback)</td>
    <td style="padding:15px">getNetworkApplicationGroupById</td>
    <td style="padding:15px">{base_path}/{version}/networkApplicationGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkApplicationGroup(groupId, callback)</td>
    <td style="padding:15px">updateNetworkApplicationGroup</td>
    <td style="padding:15px">{base_path}/{version}/networkApplicationGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkApplicationGroup(groupId, callback)</td>
    <td style="padding:15px">deleteNetworkApplicationGroup</td>
    <td style="padding:15px">{base_path}/{version}/networkApplicationGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkApplications(search, locale, callback)</td>
    <td style="padding:15px">getNetworkApplications</td>
    <td style="padding:15px">{base_path}/{version}/networkApplications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkApplicationById(appId, locale, callback)</td>
    <td style="padding:15px">getNetworkApplicationById</td>
    <td style="padding:15px">{base_path}/{version}/networkApplications/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkServiceGroups(search, callback)</td>
    <td style="padding:15px">getNetworkServiceGroups</td>
    <td style="padding:15px">{base_path}/{version}/networkServiceGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCustomNetworkServiceGroup(callback)</td>
    <td style="padding:15px">addCustomNetworkServiceGroup</td>
    <td style="padding:15px">{base_path}/{version}/networkServiceGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkServiceGroupsLite(callback)</td>
    <td style="padding:15px">getNetworkServiceGroupsLite</td>
    <td style="padding:15px">{base_path}/{version}/networkServiceGroups/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkServiceGroupById(serviceGroupId, callback)</td>
    <td style="padding:15px">getNetworkServiceGroupById</td>
    <td style="padding:15px">{base_path}/{version}/networkServiceGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editNetworkServiceGroup(serviceGroupId, callback)</td>
    <td style="padding:15px">editNetworkServiceGroup</td>
    <td style="padding:15px">{base_path}/{version}/networkServiceGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomNetowrkServiceGroup(serviceGroupId, callback)</td>
    <td style="padding:15px">deleteCustomNetowrkServiceGroup</td>
    <td style="padding:15px">{base_path}/{version}/networkServiceGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkServices(search, protocol, locale, callback)</td>
    <td style="padding:15px">getNetworkServices</td>
    <td style="padding:15px">{base_path}/{version}/networkServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCustomNetworkService(body, callback)</td>
    <td style="padding:15px">addCustomNetworkService</td>
    <td style="padding:15px">{base_path}/{version}/networkServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkServicesLite(locale, callback)</td>
    <td style="padding:15px">getNetworkServicesLite</td>
    <td style="padding:15px">{base_path}/{version}/networkServices/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkServiceById(serviceId, locale, callback)</td>
    <td style="padding:15px">getNetworkServiceById</td>
    <td style="padding:15px">{base_path}/{version}/networkServices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editNetworkService(serviceId, callback)</td>
    <td style="padding:15px">editNetworkService</td>
    <td style="padding:15px">{base_path}/{version}/networkServices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomNetworkService(serviceId, callback)</td>
    <td style="padding:15px">deleteCustomNetworkService</td>
    <td style="padding:15px">{base_path}/{version}/networkServices/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimeWindows(callback)</td>
    <td style="padding:15px">getTimeWindows</td>
    <td style="padding:15px">{base_path}/{version}/timeWindows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTimeWindowSummary(callback)</td>
    <td style="padding:15px">getTimeWindowSummary</td>
    <td style="padding:15px">{base_path}/{version}/timeWindows/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUrlFilteringRules(callback)</td>
    <td style="padding:15px">getRules</td>
    <td style="padding:15px">{base_path}/{version}/urlFilteringRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUrlFilteringRules(body, callback)</td>
    <td style="padding:15px">addRule</td>
    <td style="padding:15px">{base_path}/{version}/urlFilteringRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUrlFilteringRulesRuleId(ruleId, callback)</td>
    <td style="padding:15px">getRuleById</td>
    <td style="padding:15px">{base_path}/{version}/urlFilteringRules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUrlFilteringRulesRuleId(ruleId, body, callback)</td>
    <td style="padding:15px">updateRule</td>
    <td style="padding:15px">{base_path}/{version}/urlFilteringRules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUrlFilteringRulesRuleId(ruleId, callback)</td>
    <td style="padding:15px">deleteRule</td>
    <td style="padding:15px">{base_path}/{version}/urlFilteringRules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForwardingRules(callback)</td>
    <td style="padding:15px">getForwardingRules</td>
    <td style="padding:15px">{base_path}/{version}/forwardingRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createForwardingRule(body, callback)</td>
    <td style="padding:15px">createForwardingRule</td>
    <td style="padding:15px">{base_path}/{version}/forwardingRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getForwardingRule(ruleId, callback)</td>
    <td style="padding:15px">getForwardingRule</td>
    <td style="padding:15px">{base_path}/{version}/forwardingRules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateForwardingRule(ruleId, body, callback)</td>
    <td style="padding:15px">updateForwardingRule</td>
    <td style="padding:15px">{base_path}/{version}/forwardingRules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForwardingRule(ruleId, callback)</td>
    <td style="padding:15px">deleteForwardingRule</td>
    <td style="padding:15px">{base_path}/{version}/forwardingRules/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZpaGateways(search, appSegment, callback)</td>
    <td style="padding:15px">getZpaGateways</td>
    <td style="padding:15px">{base_path}/{version}/zpaGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addZpaGateway(body, callback)</td>
    <td style="padding:15px">addZpaGateway</td>
    <td style="padding:15px">{base_path}/{version}/zpaGateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZpaGatewayById(gatewayId, callback)</td>
    <td style="padding:15px">getZpaGatewayById</td>
    <td style="padding:15px">{base_path}/{version}/zpaGateways/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editZpaGateway(gatewayId, body, callback)</td>
    <td style="padding:15px">editZpaGateway</td>
    <td style="padding:15px">{base_path}/{version}/zpaGateways/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteZpaGateway(gatewayId, callback)</td>
    <td style="padding:15px">deleteZpaGateway</td>
    <td style="padding:15px">{base_path}/{version}/zpaGateways/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntermediateCaCertificates(page, pageSize, callback)</td>
    <td style="padding:15px">getIntermediateCaCertificates</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addIntermediateCaCertificate(body, callback)</td>
    <td style="padding:15px">addIntermediateCaCertificate</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadAttestationStmt(certId, callback)</td>
    <td style="padding:15px">downloadAttestationStmt</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/downloadAttestation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadCSR(certId, callback)</td>
    <td style="padding:15px">downloadCSR</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/downloadCsr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadHsmPublicKey(certId, callback)</td>
    <td style="padding:15px">downloadHsmPublicKey</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/downloadPublicKey/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">finalizeCert(certId, callback)</td>
    <td style="padding:15px">finalizeCert</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/finalizeCert/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateCsr(certId, body, callback)</td>
    <td style="padding:15px">generateCsr</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/generateCsr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateHsmKeyPair(certId, callback)</td>
    <td style="padding:15px">generateHsmKeyPair</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/keyPair/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntermediateCaCertificatesLite(page, pageSize, callback)</td>
    <td style="padding:15px">getIntermediateCaCertificatesLite</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntermediateCaCertificateLite(certId, callback)</td>
    <td style="padding:15px">getIntermediateCaCertificateLite</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/lite/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">makeDefaultCertificate(certId, callback)</td>
    <td style="padding:15px">makeDefaultCertificate</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/makeDefault/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertificatesInReadyToUseState(page, pageSize, callback)</td>
    <td style="padding:15px">getCertificatesInReadyToUseState</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/readyToUse?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showCert(certId, callback)</td>
    <td style="padding:15px">showCert</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/showCert/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">showCsr(certId, callback)</td>
    <td style="padding:15px">showCsr</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/showCsr/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadCert(certId, body, callback)</td>
    <td style="padding:15px">uploadCert</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/uploadCert/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uploadCertChain(certId, body, callback)</td>
    <td style="padding:15px">uploadCertChain</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/uploadCertChain/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyKeyAttestation(certId, callback)</td>
    <td style="padding:15px">verifyKeyAttestation</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/verifyKeyAttestation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIntermediateCaCertificate(certId, callback)</td>
    <td style="padding:15px">getIntermediateCaCertificate</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateIntermediateCaCertificate(certId, body, callback)</td>
    <td style="padding:15px">updateIntermediateCaCertificate</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteIntermediateCaCertificate(certId, callback)</td>
    <td style="padding:15px">deleteIntermediateCaCertificate</td>
    <td style="padding:15px">{base_path}/{version}/intermediateCaCertificate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSslExemptedUrls(callback)</td>
    <td style="padding:15px">getSslExemptedUrls</td>
    <td style="padding:15px">{base_path}/{version}/sslSettings/exemptedUrls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSslExemptedUrls(action, callback)</td>
    <td style="padding:15px">updateSslExemptedUrls</td>
    <td style="padding:15px">{base_path}/{version}/sslSettings/exemptedUrls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTypes(callback)</td>
    <td style="padding:15px">getDeviceTypes</td>
    <td style="padding:15px">{base_path}/{version}/iotDiscovery/deviceTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCategories(callback)</td>
    <td style="padding:15px">getCategories</td>
    <td style="padding:15px">{base_path}/{version}/iotDiscovery/categories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClassifications(callback)</td>
    <td style="padding:15px">getClassifications</td>
    <td style="padding:15px">{base_path}/{version}/iotDiscovery/classifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceList(callback)</td>
    <td style="padding:15px">getDeviceList</td>
    <td style="padding:15px">{base_path}/{version}/iotDiscovery/deviceList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubLocations(locationId, search, sslScanEnabled, xffEnabled, authRequired, bwEnforced, enforceAup, enableFirewall, enableIOT, callback)</td>
    <td style="padding:15px">getSubLocations</td>
    <td style="padding:15px">{base_path}/{version}/locations/{pathv1}/sublocations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsCount(lastModUser, version, name, groupType, comments, locationId, callback)</td>
    <td style="padding:15px">getGroupsCount</td>
    <td style="padding:15px">{base_path}/{version}/locations/groups/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupsSummary(page, pageSize, callback)</td>
    <td style="padding:15px">getGroupsSummary</td>
    <td style="padding:15px">{base_path}/{version}/locations/groups/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupSummary(id, callback)</td>
    <td style="padding:15px">getGroupSummary</td>
    <td style="padding:15px">{base_path}/{version}/locations/groups/lite/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGroupById(id, callback)</td>
    <td style="padding:15px">getGroupById</td>
    <td style="padding:15px">{base_path}/{version}/locations/groups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRuleLabels(page, pageSize, callback)</td>
    <td style="padding:15px">getRuleLabels</td>
    <td style="padding:15px">{base_path}/{version}/ruleLabels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRuleLabel(callback)</td>
    <td style="padding:15px">addRuleLabel</td>
    <td style="padding:15px">{base_path}/{version}/ruleLabels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRuleLabelById(ruleLabelId, callback)</td>
    <td style="padding:15px">getRuleLabelById</td>
    <td style="padding:15px">{base_path}/{version}/ruleLabels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateRuleLabel(ruleLabelId, callback)</td>
    <td style="padding:15px">updateRuleLabel</td>
    <td style="padding:15px">{base_path}/{version}/ruleLabels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRuleLabel(ruleLabelId, callback)</td>
    <td style="padding:15px">deleteRuleLabel</td>
    <td style="padding:15px">{base_path}/{version}/ruleLabels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomFileHash(callback)</td>
    <td style="padding:15px">getCustomFileHash</td>
    <td style="padding:15px">{base_path}/{version}/behavioralAnalysisAdvancedSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCustomFileHash(body, callback)</td>
    <td style="padding:15px">updateCustomFileHash</td>
    <td style="padding:15px">{base_path}/{version}/behavioralAnalysisAdvancedSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomFileHashQuota(callback)</td>
    <td style="padding:15px">getCustomFileHashQuota</td>
    <td style="padding:15px">{base_path}/{version}/behavioralAnalysisAdvancedSettings/fileHashCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkUpdateCloudApplication(body, callback)</td>
    <td style="padding:15px">bulkUpdateCloudApplication</td>
    <td style="padding:15px">{base_path}/{version}/cloudApplications/bulkUpdate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCloudApplicationsLite(pageNumber, limit, callback)</td>
    <td style="padding:15px">getCloudApplicationsLite</td>
    <td style="padding:15px">{base_path}/{version}/cloudApplications/lite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCustomTags(callback)</td>
    <td style="padding:15px">getAllCustomTags</td>
    <td style="padding:15px">{base_path}/{version}/customTags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportDataApplicationsExport(body, callback)</td>
    <td style="padding:15px">getReportDataApplicationsExport</td>
    <td style="padding:15px">{base_path}/{version}/shadowIT/applications/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getReportTransactionsExport(entity, body, callback)</td>
    <td style="padding:15px">getReportTransactionsExport</td>
    <td style="padding:15px">{base_path}/{version}/shadowIT/applications/{pathv1}/exportCsv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllGreTunnels(page, pageSize, callback)</td>
    <td style="padding:15px">getAllGreTunnels</td>
    <td style="padding:15px">{base_path}/{version}/greTunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addGreTunnel(body, callback)</td>
    <td style="padding:15px">addGreTunnel</td>
    <td style="padding:15px">{base_path}/{version}/greTunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateAndGetAvailableInternalIpRanges(internalIpRange, staticIp, limit, callback)</td>
    <td style="padding:15px">validateAndGetAvailableInternalIpRanges</td>
    <td style="padding:15px">{base_path}/{version}/greTunnels/availableInternalIpRanges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGreTunelById(id, callback)</td>
    <td style="padding:15px">getGreTunelById</td>
    <td style="padding:15px">{base_path}/{version}/greTunnels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGreTunnel(id, body, callback)</td>
    <td style="padding:15px">updateGreTunnel</td>
    <td style="padding:15px">{base_path}/{version}/greTunnels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGreTunnel(id, callback)</td>
    <td style="padding:15px">deleteGreTunnel</td>
    <td style="padding:15px">{base_path}/{version}/greTunnels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPv6Configuration(callback)</td>
    <td style="padding:15px">getIPv6Configuration</td>
    <td style="padding:15px">{base_path}/{version}/ipv6config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDns64Prefixes(search, callback)</td>
    <td style="padding:15px">getDns64Prefixes</td>
    <td style="padding:15px">{base_path}/{version}/ipv6config/dns64prefix?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNat64Prefixes(search, page, pageSize, callback)</td>
    <td style="padding:15px">getNat64Prefixes</td>
    <td style="padding:15px">{base_path}/{version}/ipv6config/nat64prefix?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPGWDetails(ipAddresses, callback)</td>
    <td style="padding:15px">getIPGWDetails</td>
    <td style="padding:15px">{base_path}/{version}/orgProvisioning/ipGreTunnelInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRegionByGeoCoordinates(latitude, longitude, callback)</td>
    <td style="padding:15px">getRegionByGeoCoordinates</td>
    <td style="padding:15px">{base_path}/{version}/region/byGeoCoordinates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRegionBySrcIPAddress(ip, callback)</td>
    <td style="padding:15px">getRegionBySrcIPAddress</td>
    <td style="padding:15px">{base_path}/{version}/region/byIPAddress/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRegionsByPrefix(prefix, page, pageSize, callback)</td>
    <td style="padding:15px">getRegionsByPrefix</td>
    <td style="padding:15px">{base_path}/{version}/region/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllStaticIPs(availableForGreTunnel, ipAddress, page, pageSize, callback)</td>
    <td style="padding:15px">getAllStaticIPs</td>
    <td style="padding:15px">{base_path}/{version}/staticIP?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addStaticIP(body, callback)</td>
    <td style="padding:15px">addStaticIP</td>
    <td style="padding:15px">{base_path}/{version}/staticIP?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateIPAddress(body, callback)</td>
    <td style="padding:15px">validateIPAddress</td>
    <td style="padding:15px">{base_path}/{version}/staticIP/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStaticIPById(id, callback)</td>
    <td style="padding:15px">getStaticIPById</td>
    <td style="padding:15px">{base_path}/{version}/staticIP/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateStaticIP(id, body, callback)</td>
    <td style="padding:15px">updateStaticIP</td>
    <td style="padding:15px">{base_path}/{version}/staticIP/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteStaticIP(id, callback)</td>
    <td style="padding:15px">deleteStaticIP</td>
    <td style="padding:15px">{base_path}/{version}/staticIP/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getZscalerNodesDetails(dc, region, page, pageSize, include, subcloud, callback)</td>
    <td style="padding:15px">getZscalerNodesDetails</td>
    <td style="padding:15px">{base_path}/{version}/vips?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRecommendedVipsGroupedByDC(routableIP, withinCountryOnly, includePrivateServiceEdge, includeCurrentVips, sourceIp, latitude, longitude, subcloud, callback)</td>
    <td style="padding:15px">getRecommendedVipsGroupedByDC</td>
    <td style="padding:15px">{base_path}/{version}/vips/groupByDatacenter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRecommendedGreVips(routableIP, withinCountryOnly, includePrivateServiceEdge, includeCurrentVips, sourceIp, latitude, longitude, subcloud, callback)</td>
    <td style="padding:15px">getRecommendedGreVips</td>
    <td style="padding:15px">{base_path}/{version}/vips/recommendedList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthExemptedUrls(callback)</td>
    <td style="padding:15px">getAuthExemptedUrls</td>
    <td style="padding:15px">{base_path}/{version}/authSettings/exemptedUrls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateAuthExemptedUrls(action, callback)</td>
    <td style="padding:15px">updateAuthExemptedUrls</td>
    <td style="padding:15px">{base_path}/{version}/authSettings/exemptedUrls?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWorkloadGroups(page, pageSize, callback)</td>
    <td style="padding:15px">getWorkloadGroups</td>
    <td style="padding:15px">{base_path}/{version}/workloadGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLocationGroups(version, name, groupType, comments, locationId, lastModUser, page, pageSize, callback)</td>
    <td style="padding:15px">getLocationGroups</td>
    <td style="padding:15px">{base_path}/{version}/locations/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUrlCategoriesReviewDomain(body, callback)</td>
    <td style="padding:15px">URL Categories - Review Domain</td>
    <td style="padding:15px">{base_path}/{version}/urlCategories/review/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putUrlCategoriesReviewDomainAddURLs(body, callback)</td>
    <td style="padding:15px">URL Categories - Review Domain - Add URLs</td>
    <td style="padding:15px">{base_path}/{version}/urlCategories/review/domains?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
