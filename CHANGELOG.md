
## 0.10.9 [10-15-2024]

* Changes made at 2024.10.14_21:32PM

See merge request itentialopensource/adapters/adapter-zscaler!38

---

## 0.10.8 [09-25-2024]

* Add missing calls and add a task to take in query data

See merge request itentialopensource/adapters/adapter-zscaler!35

---

## 0.10.7 [09-20-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-zscaler!36

---

## 0.10.6 [08-15-2024]

* fix vulnerabilities

See merge request itentialopensource/adapters/adapter-zscaler!34

---

## 0.10.5 [08-15-2024]

* Changes made at 2024.08.14_19:52PM

See merge request itentialopensource/adapters/adapter-zscaler!33

---

## 0.10.4 [08-14-2024]

* Changes made at 2024.08.14_15:36PM

See merge request itentialopensource/adapters/adapter-zscaler!32

---

## 0.10.3 [08-07-2024]

* Changes made at 2024.08.06_21:58PM

See merge request itentialopensource/adapters/adapter-zscaler!31

---

## 0.10.2 [06-27-2024]

* Updating metadata and package-lock

See merge request itentialopensource/adapters/adapter-zscaler!30

---

## 0.10.1 [05-24-2024]

* Added auth call

See merge request itentialopensource/adapters/adapter-zscaler!29

---

## 0.10.0 [05-16-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/security/adapter-zscaler!27

---

## 0.9.0 [05-02-2024]

* Add new zscaler ZIA calls

See merge request itentialopensource/adapters/security/adapter-zscaler!26

---

## 0.8.3 [03-27-2024]

* Changes made at 2024.03.27_13:55PM

See merge request itentialopensource/adapters/security/adapter-zscaler!25

---

## 0.8.2 [03-11-2024]

* Changes made at 2024.03.11_16:22PM

See merge request itentialopensource/adapters/security/adapter-zscaler!24

---

## 0.8.1 [02-27-2024]

* Changes made at 2024.02.27_11:59AM

See merge request itentialopensource/adapters/security/adapter-zscaler!23

---

## 0.8.0 [12-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-zscaler!22

---
