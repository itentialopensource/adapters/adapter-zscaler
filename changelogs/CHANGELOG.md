
## 0.7.0 [05-24-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-zscaler!13

---

## 0.6.7 [01-19-2022]

- Changed the health check url to be a url that exists in zscaler

See merge request itentialopensource/adapters/security/adapter-zscaler!12

---

## 0.6.6 [11-17-2021]

- Fix lint errors after updating adapter.js and pronghorn.json

See merge request itentialopensource/adapters/security/adapter-zscaler!10

---

## 0.6.5 [11-02-2021]

- This added the updated "getObfuscatedKey" auth to all empty body payloads and updated the calls to allow every call to be properly wrapped in a session.

See merge request itentialopensource/adapters/security/adapter-zscaler!8

---

## 0.6.4 [10-25-2021]

- Changes to still support both Basic Auth and Obfuscated ApiKey Auth

See merge request itentialopensource/adapters/security/adapter-zscaler!7

---

## 0.6.3 [03-16-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/security/adapter-zscaler!6

---

## 0.6.2 [07-09-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/security/adapter-zscaler!5

---

## 0.6.1 [01-15-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/security/adapter-zscaler!4

---

## 0.6.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/security/adapter-zscaler!3

---

## 0.5.0 [09-17-2019] & 0.4.0 [09-14-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/security/adapter-zscaler!2

---
## 0.3.0 [07-30-2019] & 0.2.0 [07-16-2019]

- Migrate the adapter to the latest foundation, categorize it and make it available to app artifact

See merge request itentialopensource/adapters/security/adapter-zscaler!1

---

## 0.1.1 [04-25-2019]

- Initial Commit

See commit 08e8fb0

---
